class Sleepnow < Formula
  desc "Sleepnow"
  homepage "https://bitbucket.org/bicarbona/sleepnow/"
  url "https://bitbucket.org/bicarbona/sleepnow/raw/master/sleepnow-latest.zip"
  version 'latest'
  ##sha256 :no_check
  sha256 "eb2fdf47837bbda7f1f961766df4955b35a87bcea88929079a36910b943696d0"
  ##version "1.0.0"

  bottle :unneeded

  def install
    bin.install "sleepnow"
    bin.install "sleepdisplay"
  end
end
